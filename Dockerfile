FROM python:3.8.3
WORKDIR .
COPY sample.py .
CMD ["python","sample.py"]
