class BinaryTree:

    def __init__(self,data):
        self.data = data
        self.left = None
        self.right=None

    def add_elements_to_tree(self,data):
        if data == self.data:
            pass
        if data < self.data:
            if  self.left:
                self.left.add_elements_to_tree(data)
            else:
                self.left = BinaryTree(data)
        if   data > self.data:
            if self.right:
                self.right.add_elements_to_tree(data)
            else:
                self.right = BinaryTree(data)

    def in_order(self):
        elements = []
        if self.left:
            elements += self.left.in_order()
        elements.append(self.data)
        if self.right:
            elements += self.right.in_order()
        return elements

    def pre_order(self):
        elements = [self.data]
        if self.left:
            elements+= self.left.pre_order()

        if self.right:
            elements+= self.right.pre_order()
        return elements

    def post_order(self):
        elements = []
        if self.left:
            elements+= self.left.post_order()

        if self.right:
            elements+= self.right.post_order()
        elements.append(self.data)
        return elements

def add_elements(elements):
    root = BinaryTree(elements[0])
    for i in range(1,len(elements)):
        root.add_elements_to_tree(elements[i])
    return root


if __name__ == '__main__':
    elements = [11,22,13,45,23,34,90,78,67,11]
    tree_list = add_elements(elements)
    print(tree_list.in_order())
    print(tree_list.pre_order())
    print(tree_list.post_order())